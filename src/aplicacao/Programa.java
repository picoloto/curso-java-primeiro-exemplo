package aplicacao;

import java.util.Locale;
import java.util.Scanner;

import entidades.Conta;

public class Programa {

	public static void main(String[] args) {

		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		Conta conta;

		System.out.print("N�mero da conta: ");
		int numero = sc.nextInt();

		System.out.print("Nome da conta: ");
		sc.nextLine();
		String nome = sc.nextLine();
		System.out.print("Criar conta com deposito inicial (s/n)?: ");
		char resposta = sc.next().charAt(0);
		if (resposta == 's') {
			System.out.print("Valor do deposito inicial (utilizar .): ");
			double depositoInicial = sc.nextDouble();
			conta = new Conta(numero, nome, depositoInicial);
		} else {
			conta = new Conta(numero, nome);
		}

		System.out.println();
		System.out.println("Dados da conta: ");
		System.out.println(conta);

		System.out.println();
		System.out.print("Valor de deposito (utilizar .): ");
		double valorDeposito = sc.nextDouble();
		conta.deposito(valorDeposito);

		System.out.println();
		System.out.println("Conta atualiza, resultado: ");
		System.out.println(conta);

		System.out.println();
		System.out.println("Ao realizar um saque � cobrada uma taxa de R$ 5.00");
		System.out.print("Valor de saque (utilizar .): ");
		double valorSaque = sc.nextDouble();
		conta.saque(valorSaque);

		System.out.println();
		System.out.println("Conta atualiza, resultado: ");
		System.out.println(conta);

		sc.close();
	}

}
